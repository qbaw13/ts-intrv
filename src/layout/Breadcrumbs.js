import React from 'react';
import {withRouter} from 'react-router-dom';
import { Breadcrumb } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

const currentPage = pathname => `
  ${pathname.charAt(1).toUpperCase()}${pathname.slice(2)} 
`;

const Breadcrumbs = ({ location = {} }) => {
  if (!location || location.pathname === '/') return null;

  return ( 
    <Breadcrumb>
      <LinkContainer to="/"><Breadcrumb.Item>Home</Breadcrumb.Item></LinkContainer>
      <Breadcrumb.Item active>{currentPage(location.pathname)}</Breadcrumb.Item>
    </Breadcrumb>
   );
};
 
export default withRouter(Breadcrumbs);