import React from 'react';
import { Navbar as BootstrapNavbar, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './Navbar.scss';

const Navbar = () => {
  return (
    <header>
      <BootstrapNavbar variant="dark" className="navbar">
        <LinkContainer to="/"><BootstrapNavbar.Brand>Logo</BootstrapNavbar.Brand></LinkContainer>
        <Nav className="mr-auto">
          <LinkContainer to="/buy"><Nav.Link>Buy</Nav.Link></LinkContainer>
          <LinkContainer to="/sell"><Nav.Link>Sell</Nav.Link></LinkContainer>
          <LinkContainer to="/about"><Nav.Link>About</Nav.Link></LinkContainer>
          <LinkContainer to="/blog"><Nav.Link>Blog</Nav.Link></LinkContainer>
        </Nav>
        <Nav>
          <LinkContainer to="/register"><Nav.Link>Sign-up</Nav.Link></LinkContainer>
          <LinkContainer to="/login"><Nav.Link>Login</Nav.Link></LinkContainer>
        </Nav>
      </BootstrapNavbar>
    </header>
 );
};
 
export default Navbar;