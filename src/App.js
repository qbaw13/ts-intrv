import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { Navbar, Breadcrumbs, Footer } from './layout';
import Register from './screens/register';
import Login from './screens/login';
import './App.scss';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="app">
          <Navbar />
          <Breadcrumbs />
          <Container className="app__content mb-2">
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
          </Container>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
