import React from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import RegisterForm from './RegisterFormContainer';
import './Register.scss';

const Register = () => ( 
  <section className="register">
    <h2>Register</h2>
    <h3><small>Create an account and get started today</small></h3>
    <Card className="register__card">
      <Card.Body>
        <Row>
          <Col md={7} className="mb-2">
            <Card>
              <Card.Body>
                <RegisterForm />
              </Card.Body>
            </Card>
          </Col>
          <Col md={5} className="register__login-tip">
            <h6>Already a member?</h6>
            <LinkContainer to="/login"><Button variant="secondary">Login</Button></LinkContainer>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  </section>
);
 
export default Register;