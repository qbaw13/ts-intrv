import React from 'react';
import { Formik } from 'formik';
import * as yup from 'yup';
import RegisterForm from './RegisterForm';
import { register } from '../../services';

const INITIAL_VALUES = Object.freeze({ 
  forename: '', 
  surname: '', 
  sex: '',
  dateOfBirth: null,
  email: '',
  verifyEmail: '',
  password: '',
});

const VALIDATION_SCHEMA = Object.freeze(yup.object({
  forename: yup.string()
    .max(100, 'Forename must contain at most 100 characters')
    .required('Forename is required'),
  surname: yup.string()
    .max(100, 'Surname must contain at most 100 characters')
    .required('Surname is required'),
  sex: yup.string()
    .oneOf(['Male', 'Female'], 'Sex declaration is required')
    .required('Sex declaration is required'),
  dateOfBirth: yup.date()
    .required('Date of birth declaration is required'),
  email: yup.string()
    .email('Invalid email format')
    .required('Email is required'),
  verifyEmail: yup.string()
    .email('Invalid email format')
    .oneOf([yup.ref("email")], 'Email does not match')
    .required('Email verification is required'),
  password: yup.string()
    .min(7, 'Surname must contain at least 7 characters')
    .max(25, 'Surname must contain at most 25 characters')
    .required('Password is required'),
}));

const handleSubmit = async (values, actions) => {
  actions.setSubmitting(true);
  await register(values);
  console.log(values);
  actions.setSubmitting(false);
};

const RegisterFormContainer = () => {
  return ( 
    <Formik
      validationSchema={VALIDATION_SCHEMA}
      initialValues={INITIAL_VALUES}
      render={props => <RegisterForm {...props} />}
      onSubmit={handleSubmit}
    />
  );
};
 
export default RegisterFormContainer;