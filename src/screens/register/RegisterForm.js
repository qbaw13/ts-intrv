import React, { Component } from "react";
import { Form, Col, InputGroup, Button, FormGroup } from "react-bootstrap";
import DatePicker from "react-datepicker";
import { shape, string, func, object, bool } from 'prop-types';
import "react-datepicker/dist/react-datepicker.css";

class RegisterForm extends Component {
  state = {
    isPasswordVisible: false
  };

  isPasswordVisible = () =>
    this.state.isPasswordVisible ? "text" : "password";

  handlePasswordVisibilityChange = () => {
    this.setState({ isPasswordVisible: !this.state.isPasswordVisible });
  };

  handleDateChange = value => {
    this.props.setFieldValue('dateOfBirth', value);
  }

  render() {
    const {
      isPasswordVisible,
      handlePasswordVisibilityChange,
      handleDateChange,
      props: {
        values: {
          forename,
          surname,
          sex,
          dateOfBirth,
          email,
          verifyEmail,
          password
        },
        errors,
        touched,
        handleChange,
        handleBlur,
        isValid,
        handleSubmit,
        isSubmitting,
      }
    } = this;

    return (
      <Form noValidate onSubmit={handleSubmit}>
        <Form.Row>
          <Form.Group as={Col} xs="12" controlId="forename">
            <Form.Label>Forename *</Form.Label>
            <Form.Control
              required
              name="forename"
              type="text"
              placeholder="Forename"
              value={forename}
              onChange={handleChange}
              onBlur={handleBlur}
              isInvalid={touched.forename && errors.forename}
            />
            <Form.Control.Feedback type="invalid">
              {errors.forename}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} xs="12" controlId="surname">
            <Form.Label>Surname *</Form.Label>
            <Form.Control
              required
              name="surname"
              type="text"
              placeholder="Surname"
              value={surname}
              onChange={handleChange}
              onBlur={handleBlur}
              isInvalid={touched.surname && errors.surname}
            />
            <Form.Control.Feedback type="invalid">
              {errors.surname}
            </Form.Control.Feedback>
          </Form.Group>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col} md="4" controlId="sex">
            <Form.Label>Sex *</Form.Label>
            <Form.Control
              as="select"
              required
              name="sex"
              value={sex}
              onChange={handleChange}
              onBlur={handleBlur}
              isInvalid={touched.sex && errors.sex}
            >
              <option>Please select</option>
              <option>Male</option>
              <option>Female</option>
            </Form.Control>
            <Form.Control.Feedback type="invalid">
              {errors.sex}
            </Form.Control.Feedback>
          </Form.Group>
          <FormGroup as={Col} md="8" controlId="dateOfBirth">
            <Form.Label>Date of birth *</Form.Label>
            <DatePicker
              className="form-control"
              calendarClassName="register__datepicker"
              name="dateOfBirth"
              placeholderText="Select date"
              selected={dateOfBirth}
              onChange={handleDateChange}
              onBlur={handleBlur}
              isInvalid={touched.dateOfBirth && errors.dateOfBirth}
              showYearDropdown
              showMonthDropdown
              autoComplete="off"
            />
          </FormGroup>
          <Form.Control.Feedback type="invalid">
            {errors.dateOfBirth}
          </Form.Control.Feedback>
        </Form.Row>
        <Form.Row>
          <Form.Group as={Col} xs="12" controlId="email">
            <Form.Label>Email Address *</Form.Label>
            <Form.Control
              required
              type="email"
              name="email"
              placeholder="Email address"
              value={email}
              onChange={handleChange}
              onBlur={handleBlur}
              isInvalid={touched.email && errors.email}
            />
            <Form.Control.Feedback type="invalid">
              {errors.email}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} xs="12" controlId="verifyEmail">
            <Form.Label>Verify Email Address *</Form.Label>
            <Form.Control
              required
              type="email"
              name="verifyEmail"
              placeholder="Verify email address"
              value={verifyEmail}
              onChange={handleChange}
              onBlur={handleBlur}
              isInvalid={touched.verifyEmail && errors.verifyEmail}
              autoComplete="off"
            />
            <Form.Control.Feedback type="invalid">
              {errors.verifyEmail}
            </Form.Control.Feedback>
          </Form.Group>
          <Form.Group as={Col} xs="12" controlId="password">
            <Form.Label>Password *</Form.Label>
            <InputGroup>
              <Form.Control
                required
                type={isPasswordVisible()}
                name="password"
                placeholder="Password"
                value={password}
                onChange={handleChange}
                onBlur={handleBlur}
                isInvalid={touched.password && errors.password}
              />
              <InputGroup.Append>
                <InputGroup.Checkbox
                  id="show-chekcbox"
                  onChange={handlePasswordVisibilityChange}
                />
                <InputGroup.Text
                  id="show-text"
                  className="register__append register__append--remove-left-border"
                >
                  Show
                </InputGroup.Text>
              </InputGroup.Append>
              <Form.Control.Feedback type="invalid">
                {errors.password}
              </Form.Control.Feedback>
            </InputGroup>
            <Form.Text className="text-muted">
              Use between 7 - 25 characters. At least one uppercase. At least
              one number.
            </Form.Text>
          </Form.Group>
        </Form.Row>
        <Button type="submit" disabled={!isValid || isSubmitting}>
          {isSubmitting? 'Sending' : 'Register'}
        </Button>
      </Form>
    );
  }
}

RegisterForm.propTypes = {
  values: shape({
    forename: string.isRequired,
    surname: string.isRequired,
    sex: string.isRequired,
    dateOfBirth: string.object,
    email: string.isRequired,
    verifyEmail: string.isRequired,
    password: string.isRequired
  }).isRequired,
  errors: object.isRequired,
  touched: object.isRequired,
  handleChange: func.isRequired,
  handleBlur: func.isRequired,
  isValid: bool.isRequired,
  handleSubmit: func.isRequired,
  isSubmitting: bool.isRequired,
}

export default RegisterForm;
