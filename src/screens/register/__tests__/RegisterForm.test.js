import React from 'react';
import { shallow as render } from 'enzyme';
import RegisterForm from '../RegisterForm';
import DatePicker from "react-datepicker";

describe('RegisterForm', () => {
  it('should render without crash', () => {
    expect(createSut().isEmptyRender()).toBe(false);
  });

  describe('forename', () => {  
    it('should call handleChange on change', () => {
      const handleChangeMock = jest.fn();
      const sut = createSut({ handleChange: handleChangeMock });

      sut.find('ForwardRef(ContextTransform)[name="forename"]').simulate('change', 'VALUE');

      expect(handleChangeMock).toHaveBeenCalledWith('VALUE');
    });
  });

  describe('datepicker', () => {
    it('should call setFieldValue with proper params on date select', () => {
      const setFieldValueMock = jest.fn();
      const sut = createSut({ setFieldValue: setFieldValueMock });

      sut.find(DatePicker).props().onChange('VALUE');

      expect(setFieldValueMock).toHaveBeenCalledWith('dateOfBirth', 'VALUE');
    });
  });

  describe('password', () => {
    let sut;

    beforeEach(() => {
      sut = createSut();
    });

    it('should be not visible by default', () => {
      expect(sut.find('ForwardRef(ContextTransform)[name="password"]').props().type).toEqual('password')
    });

    it('should change visibility on change', () => {
      const checkbox = sut.find('InputGroupCheckbox')
      
      checkbox.simulate('change');
      expect(sut.find('ForwardRef(ContextTransform)[name="password"]').props().type).toEqual('text')
    
      checkbox.simulate('change');
      expect(sut.find('ForwardRef(ContextTransform)[name="password"]').props().type).toEqual('password')
    });
  });

  describe('submitting', () => {
    it('should submit button be disabled when form is invalid', () => {
      const sut = createSut({ isValid: false });

      expect(sut.find('ForwardRef(Bootstrap(Button))').props().disabled).toBe(true);
    });

    it('should submit button be enabled when form is valid', () => {
      const sut = createSut({ isValid: true });

      expect(sut.find('ForwardRef(Bootstrap(Button))').props().disabled).toBe(false);
    });

    it('should submit form', () => {
      const handleSubmitMock = jest.fn();
      const sut = createSut({ handleSubmit: handleSubmitMock });

      sut.find('ForwardRef(Bootstrap(Form))').simulate('submit', 'VALUES', 'ACTIONS');

      expect(handleSubmitMock).toHaveBeenCalledWith('VALUES', 'ACTIONS');
    });
  });
});

const createSut = props => render(
  <RegisterForm
    values={{
      forename: '',
      surname: '', 
      sex: '',
      email: '',
      verifyEmail: '',
      password: '',

    }}
    touched={{}}
    errors={{}}
    handleChange={() => {}}
    handleBlur={() => {}}
    handleSubmit={() => {}}
    isValid={false}
    isSubmitting={false}
    {...props} />
);