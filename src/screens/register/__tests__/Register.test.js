import React from 'react';
import { shallow as render } from 'enzyme';
import Register from '../Register';

describe('Register', () => {
  
  it('should render without crash', () => {
    expect(createSut().isEmptyRender()).toBe(false);
  });
});

const createSut = () => render(<Register />);