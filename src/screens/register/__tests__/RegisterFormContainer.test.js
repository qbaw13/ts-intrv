import React from 'react';
import { shallow as render } from 'enzyme';
import RegisterFormContainer from '../RegisterFormContainer';
import { Formik } from 'formik';
import { register as registerMock } from '../../../services'; 

jest.mock('../../../services', () => ({
  register: jest.fn()
}))

describe('RegisterFormContainer', () => {
  let sut;

  beforeEach(() => {
    sut = createSut()
  });

  it('should render without crash', () => {
    expect(sut.isEmptyRender()).toBe(false);
  });

  it('should pass proper initial values', () => {
    expect(sut.find(Formik).props().initialValues).toMatchObject({
      forename: '', 
      surname: '', 
      sex: '',
      dateOfBirth: null,
      email: '',
      verifyEmail: '',
      password: '',
    });
  });

  it('should pass proper validation schema', () => {
    expect(sut.find(Formik).props().validationSchema).toMatchSnapshot()
  });

  describe('submitting', () => {
    afterEach(() => {
      jest.clearAllMocks();
    })

    it('should set submitting on true', () => {
      const setSubmittingMock = jest.fn();

      sut.find(Formik).props().onSubmit('VALUES', { setSubmitting: setSubmittingMock });

      expect(setSubmittingMock).toHaveBeenCalledWith(true);
    });

    it('should call register service on submit', async () => {
      await sut.find(Formik).props().onSubmit('VALUES', { setSubmitting: () => {} });

      expect(registerMock).toHaveBeenCalledWith('VALUES');
    });

    it('should set submitting on false', async () => {
      const setSubmittingMock = jest.fn();

      await sut.find(Formik).props().onSubmit('VALUES', { setSubmitting: setSubmittingMock });

      expect(setSubmittingMock).toHaveBeenCalledWith(false);
    });
  })
});

const createSut = () => render(<RegisterFormContainer />);